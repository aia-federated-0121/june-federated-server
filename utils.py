# data storage
import _pickle as cpkl
import bz2

# Pickle a file and then compress it into a file with extension 
"""
usage: 
    compressed_pickle('path/filename', data) 
"""

def compressed_cpickle(path_title, data):
    with bz2.BZ2File(path_title+'.pbz2', "w") as f: 
        cpkl.dump(data, f)

        
# Load any compressed pickle file
"""
usage: 
    data = decompress_pickle('example_cp.pbz2') 
"""
def decompress_cpickle(file):
    data = bz2.BZ2File(file, 'rb')
    data = cpkl.load(data)
    return data


import os
import warnings

class env_validation:
    def __init__(self):
        self.get_APY_KEY()
        self.API_KEY_valid()
        dic = self.account_setting()
        self.gitURL = dic['gitURL']
        self.account = dic['account']
    
    def get_APY_KEY(self):
        with open('./local.cfg', mode='r', encoding='UTF-8') as f:
            r = f.readlines()
            item_dict = {}
            for item in r:
                key, value = item.split('=')
                item_dict[key] = value
            self.API_KEY = item_dict['key']

    def API_KEY_valid(self):
        API_KEY = self.API_KEY
        with open('./training.cfg', mode='r', encoding='UTF-8') as f:
            r = f.readlines()
            item_dict = {}
            for item in r:
                key, value = item.split('=')
                item_dict[key] = value
                
        Participants = item_dict['trainKey'].split('/')
        FL_round = item_dict['round']
        

        if API_KEY in Participants:
            print('Training your model and push to git repo.')
            print('Your API_KEY = ',API_KEY)
            print('round=',FL_round)
            return True
        else:
            print('Your API_KEY = ',API_KEY)
            warnings.warn("You are not a participant in this round")
#             raise ValueError('You are not a participant in this round')
            return False
            
    
    def account_setting(self):
        a = os.getcwd().split('/')[-3:]
        if not a == ['FL_June', 'client', 'federated_aia_test']:
            raise ValueError('Path error')
            
            
        path = os.getcwd().split('FL_June')[0]
        path += 'FL_June/client'
        if not 'account.cfg' in os.listdir(path):
            raise ValueError('account.cfg not found.')
        print('account.cfg path: {path}'.format(path=path+'/account.cfg'))
        with open(path+'/account.cfg', 'r') as f:
            r = f.read()

            dic = {}
            for i in r.splitlines():
                i = [item.strip() for item in i.split('=')]
        #         print(i.split('='))
                dic[i[0]] = i[1]
            print('>> account setting: ',dic)
        return dic
